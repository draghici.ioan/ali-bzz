from django import forms
from dobwidget import DateOfBirthWidget

from . import models
from .models import PostPitic
from django.contrib.auth.models import User
from .models import Profile


class PostPiticForm(forms.ModelForm):
    class Meta:
        model = PostPitic
        fields = ('title', 'description', 'article', 'pub_date', 'user', 'domain', 'tags', 'pic')


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['profile_pic', 'bio', 'date_of_birth', 'hobby']
        widgets = {
            'date_of_birth': DateOfBirthWidget(),
        }
