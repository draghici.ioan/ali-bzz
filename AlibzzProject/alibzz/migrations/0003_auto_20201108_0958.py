# Generated by Django 3.1.2 on 2020-11-08 07:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alibzz', '0002_auto_20201108_0852'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='postpitic',
            options={'ordering': ['-pub_date']},
        ),
        migrations.RenameField(
            model_name='comment',
            old_name='comment_date',
            new_name='pub_date',
        ),
        migrations.RenameField(
            model_name='postpitic',
            old_name='data_posted',
            new_name='pub_date',
        ),
        migrations.RenameField(
            model_name='postpitic',
            old_name='user_name',
            new_name='user',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='comment',
        ),
        migrations.AddField(
            model_name='comment',
            name='body',
            field=models.TextField(default='SOME STRING', max_length=500),
        ),
        migrations.AlterField(
            model_name='comment',
            name='post',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='alibzz.postpitic'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='VideoPostPitic',
        ),
    ]
