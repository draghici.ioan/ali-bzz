from gtts import gTTS
import os
import time
import playsound

def speak(text):
	text = gTTS(text=text, lang='ro')
	filename = '../static/media/text_voice.mp3'
	text.save(filename)
	playsound.playsound(filename)
	
