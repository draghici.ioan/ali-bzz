from django.urls import path
from .views import home, homepitic, homevoinic, RegisterView, PostView, PostCreate, PostDelete, PostUpdate, \
    CommentCreate, profile, update_profile

from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', home, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='home')),
    path('register/', RegisterView.as_view(), name="register"),
    path('profile/', profile, name="profile"),
    path('update/', update_profile, name="update_profile"),
    path('homepitic/', homepitic, name='homepitic'),
    path('homevoinic/', homevoinic, name='homevoinic'),
    path('post/<int:pk>/', PostView.as_view(), name='post'),
    path('post/create/', PostCreate.as_view(), name='create_post'),
    path('post/create/<int:pk>/update', PostUpdate.as_view(), name='update_post'),
    path('post/<int:pk>/delete/', PostDelete.as_view(), name='delete_post'),
    path('post/<int:pk>/comment/', CommentCreate.as_view(), name='create_comment')

]
