from django.contrib import admin
from .models import Comment, PostPitic, Like, Domain, Category, Profile
# Register your models here.


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	list_display = ('pk', 'user', 'bio', 'date_of_birth', 'profile_pic', 'hobby')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
	list_display = ('pk', 'user', 'post', 'body', 'pub_date')


@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
	list_display = ('pk', 'name', 'category')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	list_display = ('pk',  'name', 'description')


@admin.register(PostPitic)
class PostAdmin(admin.ModelAdmin):
	list_display = ('pk',  'title', 'description', 'article', 'pub_date', 'user', 'domain', 'tags', 'pic')


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
	list_display = ('pk',  'user', 'post')




