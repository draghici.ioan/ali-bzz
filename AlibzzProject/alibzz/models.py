from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone


# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]


class Domain(models.Model):
    name = models.CharField(max_length=20)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class PostPitic(models.Model):
    title = models.CharField(max_length=150)
    description = models.CharField(max_length=300, help_text='Introduce o scurta descriere')
    article = models.TextField(help_text='Introduce continutul articolului', null=False)
    pub_date = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.SET_NULL, null=True)
    tags = models.CharField(max_length=100, blank=True)
    pic = models.ImageField(upload_to='static/images/post_pitic_image/', blank=True)

    class Meta:
        ordering = ["-pub_date"]

    def get_absolute_url(self):
        return reverse('post', kwargs={'pk': self.pk})

    def __str__(self):
        return '"{title}" by {username}'.format(title=self.title,
                                                username=self.user.username)


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(PostPitic, on_delete=models.CASCADE)
    body = models.TextField(max_length=500, default='SOME STRING')
    pub_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '"{body}..." on {post_title} by {username}'.format(body=self.body[:20],
                                                                  post_title=self.post.title,
                                                                  username=self.user.username)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    post = models.ForeignKey(PostPitic, on_delete=models.CASCADE, related_name='likes')


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.CharField(null=True, max_length=200)
    date_of_birth = models.DateField(null=True, blank=True)
    profile_pic = models.ImageField(default='default.jpg', upload_to='static/profiles_pics')
    hobby = models.CharField(null=True, max_length=200)


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
