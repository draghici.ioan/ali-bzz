from datetime import date

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy, reverse
from django.views import generic

from .forms import ProfileUpdateForm, UserUpdateForm
from .models import Category, Domain, PostPitic, Comment, Profile
from django.views.generic import ListView, CreateView, UpdateView, DeleteView


# Create your views here.
def home(request):
    return render(request, 'home.html')


def homepitic(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    if restrict(profile.date_of_birth, 7):
        return render(request, 'pitic_template/home_pitic.html')
    else:
        pass


def homevoinic(request):
    return render(request, 'voinic_template/home_voinic.html')


class RegisterView(CreateView):
    model = User
    fields = ['username', 'password', 'first_name', 'last_name', 'email']
    template_name = 'registration.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.instance.password = make_password(form.instance.password)
        return super().form_valid(form)


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your Profile has been Updated Successfully')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
        context = {
            'u_form': u_form,
            'p_form': p_form
        }
    return render(request, 'profile.html', context)


def update_profile(request, user_id):
    user = User.objects.get(pk=user_id)
    user.profile.hobby = ''
    user.save()


class CategoryListView(ListView):
    model = Category
    # template_name = 'home.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.objects.order_by('pk')


class DomainListView(ListView):
    model = Domain
    # template_name = 'home.html'
    context_object_name = 'domaines'


class PostView(generic.DetailView):
    model = PostPitic
    template_name = 'post.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comments = Comment.objects.filter(post=self.kwargs['pk'])
        context['comments'] = comments
        return context


class PostCreate(LoginRequiredMixin, CreateView):
    model = PostPitic
    fields = ['title', 'body']
    template_name = 'create_post.html'
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class PostUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = PostPitic
    fields = ['title', 'body']
    template_name = 'create_post.html'
    login_url = reverse_lazy('login')

    def test_func(self):
        return PostPitic.objects.get(id=self.kwargs['pk']).user == self.request.user


class PostDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = PostPitic
    success_url = reverse_lazy('home')
    login_url = reverse_lazy('login')

    def test_func(self):
        return PostPitic.objects.get(id=self.kwargs['pk']).user == self.request.user


class CommentCreate(LoginRequiredMixin, CreateView):
    model = Comment
    fields = ['body']
    template_name = 'create_comment.html'
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.post = PostPitic.objects.get(id=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('post', kwargs={'pk': self.kwargs['pk']})


def restrict(user_birth_date, required_age):
    today = date.today()
    if (user_birth_date.year + required_age, user_birth_date.month, user_birth_date.day) > (
            today.year, today.month, today.day):
        return True
    else:
        return False
